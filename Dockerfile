FROM openjdk:8-jdk-alpine
#ARG JAR_FILE=target/*.jar
COPY target/*.jar api-gw.jar
EXPOSE 9090

ENTRYPOINT ["java","-jar","/api-gw.jar"]
